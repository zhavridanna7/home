<?php

require 'config_init.php';

//для входа в свой кабинет если не авторизирован
if(empty($_COOKIE['user'])) {
    $rendered_flats  ='<section class="container">
      <div class="pricing-header p-3 pb-md-4 mx-auto text-center">
        <h1 class="display-4 fw-normal">Аренда недвижимоcтей в Минске</h1>
        <p class="fs-5 text-muted mt-4">Для того, чтобы снять недвижимость для аренды,<br>пожалуйста, зайдите в кабинет</p>
      </div>
    </section>';
} else {
    $rendered_flats = render_flat_rent_cards();
}

?>

<!DOCTYPE html>
<html lang="ru">
    <?php render_head() ?>
<body>
  <?php render_header('spisok.php') ?>

  <?php echo $rendered_flats; ?>

  <?php render_footer() ?>

</body>
</html>