<?php

function create_flat($flat_data){
    $query_params = '"' . implode('","', $flat_data) . '"';
    $query = "INSERT INTO `flats` (`u_id`, `street`, `place`, `size`, `price`, `img`) VALUES ($query_params)";
    return run_query($query);
}

/// Get functions

function get_all_rented_flats($userid){
    $query = "SELECT f.* 
              FROM `flats` f
              LEFT JOIN `user_rent` ur ON ur.flatid = f.id
              WHERE ur.userid = $userid";
    return fetch_all_query($query);
}

function get_all_unrented_flats($userid){
    $query = "SELECT f.* 
              FROM `flats` f
              LEFT JOIN `user_rent` ur ON ur.flatid = f.id AND ur.userid = $userid
              WHERE ur.id IS NULL AND f.u_id != $userid";
    return fetch_all_query($query);
}

function get_user_flats($userid){
    $mysqli = get_connection();
    $mysqli_result = mysqli_query($mysqli, "SELECT * FROM `flats` WHERE u_id = $userid");
    return mysqli_fetch_all($mysqli_result, MYSQLI_ASSOC);
}

/// Render functions

function render_flat_cards($flats = []){
    $template = 'flat_card';
    if (empty($flats)){
        $flats = get_all_unrented_flats($_COOKIE['user']  ?: -1);
    }
    $output = '';
    foreach ($flats as $flat){
        $output .= render_template($template, $flat);
    }
    return $output;
}

function render_flat_rent_cards($flats = []){
    $template = 'flat_rent';
    if (empty($flats)){
        $flats = get_all_rented_flats($_COOKIE['user']);
    }

    return render_template($template, $flats);
}

function render_flat_rent_card($flat){
    $template = 'flat_rent_card';
    return render_template($template, $flat);
}

function render_own_flats(){
    if (empty($_COOKIE['user'])){
        die('You are not authorised');
    }
    $template = 'own_flat';
    $flats = get_user_flats($_COOKIE['user']);

    $output = '';
    foreach ($flats as $flat){
        $output .= render_template($template, $flat);
    }
    return $output;
}