<?php

function render_template($template_name, $params = []){
    static $templates = [];
    if(!isset($templates[$template_name])){
        $templates[$template_name] = get_template_path($template_name);
    }

    ob_start();
    include($templates[$template_name]);
    $output = ob_get_contents();
    ob_end_clean();

    return $output;
}

function get_template_path($template_name){
    global $CONFIG;
    return $CONFIG->dir.'/templates/'.$template_name.'.php';
}

function render_head($print = true){
    return print_or_render_template('head', $print);
}

function render_header($current_url = 'index.php', $print = true){
    $data = ['current_url' => $current_url];
    return print_or_render_template('header', $print, $data);
}
function render_footer($print = true){
    return print_or_render_template('footer', $print);
}

function print_or_render_template($template, $print = false, $data = []){
    $output = render_template($template, $data);
    if ($print){
        echo $output;
        return '';
    } else {
        return $output;
    }
}

function render_simple_page($content){
    return '
<!DOCTYPE html>
<html lang="ru">
    '.render_head(false).'
<body>
    '.render_header(null, false).'
    
    '.$content.'
    
    '.render_footer(false).'
</body>
</html>';
}