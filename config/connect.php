<?php

function get_connection($force = false){
    static $mysqli = null;
    if(!is_null($mysqli) || $force){
        return $mysqli;
    }

    $dbhost = 'localhost';
    $dbname = 'home';
    $dbuser = 'root';
    $dbpass = 'root';
    $dbport = 3307;
    mysqli_report(MYSQLI_REPORT_STRICT);
    $mysqli = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname, $dbport);
    if(empty($mysqli)){
        die('Wrong DB connection');
    }
    return $mysqli;
}

/**
 * Runs SQL query without output (INSERT|DELETE|UPDATE)
 *
 * @param $query
 *
 * @return bool|mysqli_result
 */
function run_query($query){
    $mysqli = get_connection();
    $result = $mysqli->query($query);
    if(!$result){
        echo($mysqli->error);
    }
    return $result;
}

/**
 * Run 'SELECT' query
 *
 * @param $query
 *
 * @return array
 */
function fetch_all_query($query){
    $mysqli = get_connection();
    $mysqli_result = $mysqli->query($query);
    if(!$mysqli_result){
        echo $mysqli->error;
        return [];
    }
    return mysqli_fetch_all($mysqli_result, MYSQLI_ASSOC);
}