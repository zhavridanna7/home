<?php

require 'config_init.php';

$rendered_flats = render_flat_cards();
?>

<!DOCTYPE html>
<html lang="ru">
    <?php render_head() ?>
<body>
    <?php render_header() ?>


  <section class="container">
    <div class="pricing-header p-3 pb-md-4 mx-auto text-center">
      <h1 class="display-4 fw-normal">Аренда недвижимости в Минске</h1>
      <p class="fs-5 text-muted mt-4">Сейчас актуально:</p>
    </div>
  </section>
  <section class="container">
    <div class="row row-cols-1 row-cols-md-3 mb-3 ">
        <?php echo $rendered_flats ?>
    </div>
  </section>

    <?php render_footer() ?>
</body>
</html>