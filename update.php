<?php

require 'config_init.php';


$flat_id = $_GET['id'];
$flat = fetch_all_query("SELECT * FROM `flats` WHERE `id` = '$flat_id'");
$flat = reset($flat);
?>


<!DOCTYPE html>
<html lang="ru">
<?php render_head() ?>
<body>
  <?php render_header('update.php') ?>

  // TODO: create a template for this file
  <?php
    if($_COOKIE['user'] == ''): //для входа в свой кабинет
  ?>
    <?php else: ?>
    <form action="vendor/update.php" method="POST">
      <div class="modal position-static overflow-unsert h-auto d-block py-5" tabindex="-1" role="dialog" id="modalTour">
        <div class="modal-dialog" role="document">
          <div class="modal-content rounded-4 shadow-lg">
            <div class="modal-body p-5">
              <h3 class="fw-bold mb-0">Изменить недвижимость для аренды:</h3>
              <ul class="d-grid gap-4 my-5 list-unstyled">
              <input type="hidden" name="id" value="<?=$flat['id']?>">
                <li class="d-flex gap-4">
                  <div class="w-100">
                    <h5 class="mb-0">Адрес:</h5>
                    <div class="mt-2">
                      <input type="text" class="form-control rounded-3" name="street" value="<?=$flat['street']?>">
                    </div>
                  </div>
                </li>
                <li class="d-flex gap-4">
                  <div class="w-100">
                    <h5 class="mb-0">Станция метро:</h5>
                    <div class="mt-2">
                      <input type="text" class="form-control rounded-3" name="place" value="<?=$flat['place']?>">
                    </div>
                  </div>
                </li>
                <li class="d-flex gap-4">
                  <div class="w-100">
                    <h5 class="mb-0">Фото квартиры:</h5>
                    <form class="mt-2" action="./addFile.php" method="POST" enctype="multipart/form-data">
                        <input type="file" name="photo">
                        <button type="button" class="btn btn-lg btn-primary mt-2 w-50" data-bs-dismiss="modal">Загрузить</button>
                        <!-- загружать по кнопке добавить -->
                      </form>
                  </div>
                </li>
                <li class="d-flex gap-4">
                  <div class="w-100">
                    <h5 class="mb-0">Количество комнат:</h5>
                    <div class="mt-2">
                      <input type="num" class="form-control rounded-3" name="size" value="<?=$flat['size']?>">
                    </div>
                  </div>
                </li>
                <li class="d-flex gap-4">
                  <div class="w-100">
                    <h5 class="mb-0">Арендная плата в месяц (byn):</h5>
                    <div class="mt-2">
                      <input type="num" class="form-control rounded-3" name="price" value="<?=$flat['price']?>">
                    </div>
                  </div>
                </li>
              </ul>
              <button type="submit" class="btn btn-lg btn-primary mt-2 w-100" data-bs-dismiss="modal">Сохранить</button>
            </div>
          </div>
        </div>
      </div>
    </form>
 <?php endif;?>

  <?php render_footer() ?>
</body>
</html>