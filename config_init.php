<?php

global $CONFIG;
$CONFIG = new stdClass;
$CONFIG->dir = __DIR__ ;

require_once 'config/connect.php';
require_once 'lib/lib.php';
require_once 'lib/flat_lib.php';

/**
 * Make a custom shutdown handler, which called when php script end execution
 */
function _shutdown_handler(){
    // close mysqli connection
    $mysqli = get_connection();
    if(!empty($mysqli)){
        $mysqli->close();
    }
}

register_shutdown_function('_shutdown_handler');
