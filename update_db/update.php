<?php
/// Test file which contains update db query
require '../config_init.php';

$update_query = 'CREATE TABLE `home`.`user_rent` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `userid` INT(10) NOT NULL , `flatid` INT(10) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;';

run_query($update_query);
header('Location: ../index.php');