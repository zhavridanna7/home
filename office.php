<?php
require 'config_init.php';

if(empty($_COOKIE['user'])) {
    $rendered_flats  ='<section class="container">
      <div class="pricing-header p-3 pb-md-4 mx-auto text-center">
        <h1 class="display-4 fw-normal">Аренда недвижимоcтей в Минске</h1>
        <p class="fs-5 text-muted mt-4">Для того, чтобы добавить недвижимость в список арендуемых,<br>пожалуйста, зайдите в кабинет</p>
      </div>
    </section>';
} else {
    //TODO: move it into template
    $rendered_flats = '<section class="container" >
      <div class="pricing-header p-3 pb-md-4 mx-auto">
        <p class="fw-bold fs-5 text-dark mt-4">Мои предложения:</p>
      </div>
      <div class="row row-cols-1 row-cols-md-3 mb-3 ">';
      $rendered_flats .= render_own_flats();
      $rendered_flats .= '</div></section>';
      $rendered_flats .= render_template('flat_form');
}
?>

<!DOCTYPE html>
<html lang="ru">
  <?php render_head() ?>
<body>
  <?php render_header('office.php') ?>

  <?php echo $rendered_flats; ?>

  <?php render_footer() ?>

</body>
</html>