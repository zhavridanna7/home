<?php

require_once '../config_init.php';

$userid = $_COOKIE['user'];
if(empty($userid)){
    echo render_simple_page('You are not authorised');
    die;
}

// Do not change params order
$flat_data = [
    'u_id' => $_COOKIE['user'],
    'street' => $_POST['street'],
    'place' => $_POST['place'],
    'size' => $_POST['size'],
    'price' => $_POST['price'],
];

$flat_data['img'] = !empty($_POST['photo']) ? $_POST['photo'] : '';

$result = create_flat($flat_data);
if(!$result){
    echo render_simple_page('Cannot create flat');
    die;
}
header('Location: /');