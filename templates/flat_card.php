<?php
/** @var array $params  */
$flat = $params;
?>

<div class="col">
    <div class="card mb-4 rounded-3 shadow-sm">
        <div class="card-header">
            <h4 class="my-0 fw-normal fs-5"><?php echo $flat['street'] ?></h4>
            <ul class="list-unstyled mt-3">
                <li><img src="https://content.kufar.by/static/frontend/svg/metro_v2.svg" alt="metro"><?php echo $flat['place'] ?></li>
            </ul>
        </div>
        <div class="card-body ">
            <div class="text-center"><?php echo $flat['img'] ?></div>
            <ul class="list-unstyled mt-3 mb-4">
                <li><?php echo $flat['size'] ?>- комнатная</li>
                <li><?php echo $flat['price'] ?> за месяц</li>
            </ul>
            <a href="vendor/rent.php?id=<?php echo $flat['id'] ?>">
                <button type="button" class="w-100 btn btn-lg btn-outline-primary" <?php
                if(empty($_COOKIE['user'])) echo'disabled'?> >Арендовать</button>
            </a>
        </div>
    </div>
</div>
