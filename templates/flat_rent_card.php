<?php
/**
 * @var array $params
 */
$flat = $params;
?>

<form action="./vendor/create.php">
<div class="col">
  <div class="card mb-4 rounded-3 shadow-sm">
    <h4 class="d-none"><?php echo $flat['id'] ?></h4>
    <div class="card-header">
      <h4 class="my-0 fw-normal fs-5"><?php echo $flat['street'] ?></h4>
      <ul class="list-unstyled mt-3">
        <li><img src="https://content.kufar.by/static/frontend/svg/metro_v2.svg" alt="metro"><?php echo $flat['place'] ?></li>
      </ul>
    </div>
    <div class="card-body ">
      <div class="text-center"><?php echo $flat['img'] ?></div>
      <ul class="list-unstyled mt-3 mb-4">
        <li><?php echo $flat['size'] ?>- комнатная</li>
        <li><?php echo $flat['price'] ?> за месяц</li>
      </ul>
      <a href="vendor/unrent.php?id=<?php echo $flat['id']?>">
        <button type="button" class="mt-2 w-100 btn btn-lg btn-outline-danger">Отказаться от аренды</button>
      </a>
    </div>
  </div>
</div>
</form>