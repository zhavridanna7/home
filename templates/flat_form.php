<?php
?>

<form action="vendor/create.php" method="POST">
    <div class="modal position-static d-block py-5" tabindex="-1" role="dialog" id="modalTour">
        <div class="modal-dialog" role="document">
            <div class="modal-content rounded-4 shadow-lg">
                <div class="modal-body p-5">
                    <h3 class="fw-bold mb-0">Добавить мою квартиру для аренды:</h3>
                    <ul class="d-grid gap-4 my-5 list-unstyled">
                        <li class="d-flex gap-4">
                            <div class="w-100">
                                <h5 class="mb-0">Адрес:</h5>
                                <div class="mt-2">
                                    <input type="text" class="form-control rounded-3" name="street">
                                </div>
                            </div>
                        </li>
                        <li class="d-flex gap-4">
                            <div class="w-100">
                                <h5 class="mb-0">Станция метро:</h5>
                                <div class="mt-2">
                                    <input type="text" class="form-control rounded-3" name="place">
                                </div>
                            </div>
                        </li>

                        <li class="d-flex gap-4">
                            <div class="w-100">
                                <h5 class="mb-0">Количество комнат:</h5>
                                <div class="mt-2">
                                    <input type="num" class="form-control rounded-3" name="size">
                                </div>
                            </div>
                        </li>
                        <li class="d-flex gap-4">
                            <div class="w-100">
                                <h5 class="mb-0">Арендная плата в месяц:</h5>
                                <div class="mt-2">
                                    <input type="num" class="form-control rounded-3" name="price">
                                </div>
                            </div>
                        </li>
                    </ul>
                    <button type="submit" class="btn btn-lg btn-primary mt-2 w-100" data-bs-dismiss="modal">Добавить</button>
                </div>
            </div>
        </div>
    </div>
</form>