<?php
/**
 * @var array $params
 */
$current_url = $params['current_url'];
?>

<header class="p-3 bg-dark text-white">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="/"><img class="logo" src="/img/logo_white.svg"></a></li>
                <?php
                $links = [
                    'index.php'     => 'Главная',
                    'office.php'    => 'Добавить в аренду',
                    'spisok.php'    => 'Список арендуемых',
                    'licenziya.php' => 'Лицензия',
                    'exit.php'      => 'Exit',
                ];
                foreach ($links as $link => $text){
                    $link_class = ($link == $current_url) ? 'text-secondary' : 'text-white';
                    echo '<li><a href="/'.$link.'" class="nav-link px-2 '.$link_class.'">'.$text.'</a></li>';
                }
                ?>
            </ul>

            <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
                <input type="search" class="form-control form-control-dark" placeholder="Search..." aria-label="Search">
            </form>

            <?php
            if (empty($_COOKIE['user'])){
                echo '<div class="text-end" >
                    <button type = "button" class="btn btn-outline-light me-2 reg" > Register</button >
                    <button type = "button" class="btn btn-warning sign login" > Login</button >
                </div>';
            } else{
                // echo 'Exit' Button
            }
            ?>
        </div>
    </div>
</header>
