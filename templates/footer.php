<?php

if(!empty($_COOKIE['username'])) {
    echo '<p class="text-center text-muted">Здравствуйте, '.$_COOKIE['username'].'. Чтобы выйти из своего аккаунта <a href="/exit.php">здесь</a>.</p>';
}

?>

<footer class="py-3 my-4 d-flex justify-content-center">
    <img class="logo" src="/img/logo_black.svg">
    <p class="text-muted">© 2022 Company, Zhavrid</p>
</footer>

<div class="" role="dialog" id="modalSignin">
    <div id="divWin">
        <div class="modal-dialog" role="document">
            <div class="modal-content rounded-4 shadow">
                <div class="modal-header p-5 pb-4 border-bottom-0">
                    <h1 class="fw-bold mb-0 fs-2 text-dark">Sign up</h1>
                    <button type="button" class="btn-close close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-5 pt-0">
                    <form action="/validation-form/check.php" method="POST">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control rounded-3" name="login" id="floatingInput" placeholder="Login">
                            <label class="floatingInput text-dark">Login</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control rounded-3" name="name" id="floatingName" placeholder="Name">
                            <label class="floatingInput text-dark">Name</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="password" class="form-control rounded-3" name="pass" id="floatingPassword" placeholder="Password">
                            <label class="floatingPassword text-dark">Password</label>
                        </div>
                        <button class="w-100 mb-2 btn btn-lg rounded-3 btn-primary" type="submit">Register</button>
                        <small class="text-muted">By clicking Sign up, you agree to the terms of use.</small>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="" role="dialog" id="modalSignins">
    <div id="divWin">
        <div class="modal-dialog" role="document">
            <div class="modal-content rounded-4 shadow">
                <div class="modal-header p-5 pb-4 border-bottom-0">
                    <h1 class="fw-bold mb-0 fs-2 text-dark login">Login</h1>
                    <button type="button" class="btn-close closes" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-5 pt-0">
                    <form action="/validation-form/auth.php" method="POST">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control rounded-3" name="login" id="floatingInput" placeholder="Login">
                            <label class="floatingInput text-dark">Login</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="password" class="form-control rounded-3" name="pass" id="floatingPassword" placeholder="Password">
                            <label class="floatingPassword text-dark">Password</label>
                        </div>
                        <button class="w-100 mb-2 btn btn-lg rounded-3 btn-primary" type="submit">Sign up</button>
                        <small class="text-muted">By clicking Sign up, you agree to the terms of use.</small>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>