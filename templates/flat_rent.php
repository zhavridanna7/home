<?php
/**
 * @var array $params
 */
$flats = $params;
?>

<?php if(empty($flats)){
    echo '<section class="container" >
  <div class="pricing-header p-3 pb-md-4 mx-auto">
    <p class="fw-bold fs-5 text-dark mt-4">You do not have rented flats</p>
  </div>
  </section>';
    return;
}
?>
<section class="container" >
  <div class="pricing-header p-3 pb-md-4 mx-auto">
    <p class="fw-bold fs-5 text-dark mt-4">Мой список арендуемых помещений:</p>
  </div>
  <div class="row row-cols-1 row-cols-md-3 mb-3 ">

  <?php
    foreach ($flats as $flat){
        echo render_flat_rent_card($flat);
    }
  ?>
  </div>
</section>
